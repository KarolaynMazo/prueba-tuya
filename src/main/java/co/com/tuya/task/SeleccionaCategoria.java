package co.com.tuya.task;

import co.com.tuya.userinterfaces.Index;
import lombok.AllArgsConstructor;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.tuya.userinterfaces.LaptosPage.SCROLL;
import static co.com.tuya.utils.Constantes.DIEZ;
import static co.com.tuya.utils.Constantes.TRENTA;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

@AllArgsConstructor
public class SeleccionaCategoria implements Task {
    private String categoria;


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(Index.categorias(categoria), isVisible()).forNoMoreThan(DIEZ).seconds(),
                Click.on(Index.categorias(categoria)),
                WaitUntil.the(Index.categorias(categoria), isVisible()).forNoMoreThan(DIEZ).seconds(),
                Scroll.to(SCROLL),
                WaitUntil.the(Index.categorias(categoria), isVisible()).forNoMoreThan(TRENTA).seconds()
        );

    }

    public static SeleccionaCategoria laCategoria(String Categoria) {
        return Tasks.instrumented(SeleccionaCategoria.class, Categoria);
    }
}
