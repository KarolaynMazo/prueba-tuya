package co.com.tuya.userinterfaces;

import co.com.tuya.utils.Utilitarios;
import net.serenitybdd.screenplay.targets.Target;

public class Index {
    public static Target categorias(String categoria) {
        return Target.the("selecciona una categoria de la lista")
                .locatedBy(Utilitarios.reemplazar("//a[contains(text(),'%s')]", categoria));
    }


}


