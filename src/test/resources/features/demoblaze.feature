#language: es
#Author: Karolayn.mazo@gmail.com

Característica: Yo como analista de automatizacion
  requiero ver los productos de la categoria laptos

  Antecedentes:
    Dado que 'cliente' desea navegar a la pagina Demoblaze

  Escenario: Cliente desea ir a la categoria de laptos
    Cuando el hace la seleccion de la "Laptops"
    Entonces deberia ver una lista de productos

@Segundo
  Esquema del escenario:  Cliente desea ir a a la categoria y seleccionar un producto
    Cuando seleccionar la Categoria y selecciona un Producto
      | Categoria   | Producto   | Precio   | Descripcion   |
      | <Categoria> | <Producto> | <Precio> | <Descripcion> |
    Entonces verifico nombre del
      | Producto   | Precio   | Descripcion   |
      | <Producto> | <Precio> | <Descripcion> |
    Ejemplos:
      | Categoria | Producto            | Precio | Descripcion                                                                                                                                                                                                                                                                                                                                           |
      | Laptops   | MacBook air         | 700    | 1.6GHz dual-core Intel Core i5 (Turbo Boost up to 2.7GHz) with 3MB shared L3 cache Configurable to 2.2GHz dual-core Intel Core i7 (Turbo Boost up to 3.2GHz) with 4MB shared L3 cache.                                                                                                                                                                |
      | Laptops   | 2017 Dell 15.6 Inch | 700    | 7th Gen Intel Core i7-7500U mobile processor 2.70 GHz with Turbo Boost Technology up to 3.50 GHz, Intel HD Graphics 62015.6 inch Full HD IPS TrueLife LED-backlit touchscreen (1920 x 1080), 10-finger multi-touch support, 360° flip-and-fold design,8GB DDR4 2400 MHz Memory, 1TB 5400 RPM HDD, No optical drive, 3 in 1 card reader (SD SDHC SDXC) |

